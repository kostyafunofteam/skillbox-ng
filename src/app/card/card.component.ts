import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <app-button></app-button>
    <app-bage></app-bage>
    <app-rating></app-rating>
  `,
  styles: [
  ]
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
