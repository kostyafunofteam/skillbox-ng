import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconTooltipComponent } from './icon-tooltip.component';
import {TooltipModule} from '../tooltip/tooltip.module';
import {IconModule} from '../icon/icon.module';



@NgModule({
  declarations: [
    IconTooltipComponent
  ],
  imports: [
    CommonModule,
    TooltipModule,
    IconModule
  ]
})
export class IconTooltipModule { }
